<!DOCTYPE html>
<html>
<head>
	<title>Login Page</title>
</head>
<body>
	<div class="container">
		<?php
	    	include 'navbar.php';
	    ?>
	    <br>
		<div class="col-md-6"  style="width:1000px; margin:0 auto;">
			<div class="card">
				<div class="card-header  bg-info text-white text-center">
					Login
				</div>
				<div class="card-body">
					<form class="form-group" action="#" method="POST">
						<b>Username: </b><input type="text" name="uname" required class="form-control col-md-6">
						<b>Password: </b><input type="password" name="password" required class="form-control col-md-6">
						<br>
						<input type="submit" name="subBtn" value="Login" class="btn btn-success">
						<input type="reset" name="resetBtn" value="Reset" class="btn btn-danger">
					</form>
					<br>
					<a href="signup_page.php">Not a User? Click here to Register</a>
				</div>
			</div>
		</div>
	</div>
		
</body>
<?php
	if (isset($_POST['subBtn']))
	{
		$user = $_POST['uname'];
		$password = $_POST['password'];

		$data = $link->prepare( 'SELECT password FROM users WHERE username = (:id) LIMIT 1;' );
		$data->bindParam( ':id', $user);
		$data->execute();
		$sql_result = $data->fetch(PDO::FETCH_ASSOC);
		// var_dump($sql_result);
		// foreach ($sql_result as $key => $value)
		// {
		// 	echo "$key ==> $value";
		// }
		// die();
		// $sql="SELECT * FROM users WHERE username='$user'";
		// $sql_result=mysqli_query($link, $sql) or exit("Sql Error");
		if (sizeof($sql_result) == 1) {
			foreach ($sql_result as $key => $value) {
				if ($value == $password) {
					session_start();
					$_SESSION["uname"] = $user;
					// print_r($_SESSION);
					echo "<script>location.href = 'home.php';</script>";
				} else {
					echo '<div class="alert alert-danger alert-fixed" role="alert">
						Password Not Matching with DB
					</div>';
				}
			}
		} else {
			echo '<div class="alert alert-danger alert-fixed" role="alert">
					No User Available
				</div>';
		}
	}
?>
</html>