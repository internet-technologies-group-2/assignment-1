<!DOCTYPE html>
<html>
<head>
	<title>Posts Page</title>
</head>
<body>
	<div class="container">
		<?php
			include 'navbar.php';

			$data = $link->prepare( 'SELECT * FROM posts' );
			$data->execute();
			$sql_result = $data->fetchall();

			if (sizeof($sql_result) < 1) {
				echo '<div class="alert alert-danger" role="alert">
						No Posts Available
					</div>';
			}
		?>
		<br>
		<div class="col-md-12"  style="width:1000px; margin:0 auto;">
			<div class="card">
				<div class="card-header  bg-info text-white text-center">
					Posts
					<a href="post_create.php" class="btn btn-success float-right">Create New Post</a>
				</div>
				<div class="card-body">
					<?php
						foreach ($sql_result as $value) {							
							echo '<a href="view_post.php?post_id='.$value['post_id'].'">'.$value['post_title'].'  -  created by '.$value['post_created_by'].'</a><br>';
						}
					?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>