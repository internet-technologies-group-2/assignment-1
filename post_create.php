<!DOCTYPE html>
<html>
<head>
	<title>Create Post</title>
</head>
<body>
	<div class="container">
		<?php
			include 'navbar.php';
		?>
		<br>
		<div class="col-md-6"  style="width:1000px; margin:0 auto;">
			<div class="card">
				<div class="card-header  bg-info text-white text-center">
					Create Post
				</div>
				<div class="card-body">
					<form class="form-group" action="#" method="POST">
						<b>Title: </b><input type="text" name="title" required class="form-control col-md-12">
						<b>Description: </b>
						<textarea rows="4" cols="50"  name="description" required class="form-control col-md-12"></textarea>	
						<br>
						<input type="submit" name="subBtn" value="Submit" class="btn btn-success">
						<a href="posts.php" class="btn btn-danger float-right">Cancel</a>
					</form>
				</div>
			</div>
		</div>
	</div>
	<?php
		if (isset($_POST['subBtn']))
		{
			$title = $_POST['title'];
			$description = $_POST['description'];
			$created_by = $display_name ;

			$statement = $link->prepare("INSERT INTO `posts` (`post_title`, `post_description`, `post_created_by`) VALUES (:user, :password, :dob); ");
			$insert_result = $statement->execute(['user' => $title, 'password' => $description, 'dob' => $created_by]);
			if($insert_result)
			{
				echo '<div class="alert alert-success alert-fixed" role="alert">
							Successfully created post!
						</div>';
			}
			else
			{
				echo '<div class="alert alert-danger alert-fixed" role="alert">
							Failed to create post!
							<br>
							Please Try Again
						</div>';
			}
			echo '<script>location.href = "posts.php";</script>';
		}
	?>
</body>
</html>