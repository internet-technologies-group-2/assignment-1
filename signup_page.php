<!DOCTYPE html>
<html>
<head>
	<title>Registration Page</title>
	<script type="text/javascript">
		function checkPassword() {
			var pass = document.getElementById('pass').value;
			var pass_again = document.getElementById('pass_again').value;
			var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

			if (pass != pass_again) {
				alert("Passwords Not matching");
				return false;
			}

			if (pass.length < 6) {
				alert("Password should be Minimum 6 characters");
				return false;
			}

			if(!regularExpression.test(pass)) {
				alert("password should contain atleast one number and one special character");
				return false;
			}

			var uname = document.getElementById('uname').value;
			if (uname.length < 6) {
				alert("Username should be Minimum 6 characters");
				return false;
			}

			var mobile_no = document.getElementById('ph_no').value;

			if (mobile_no.length != 10) {
				alert("Mobile Number should be 10 digits");
				return false;
			}

			return true;
		}
	</script>
</head>
<body>
	<div class="container">
		<?php
	    	include 'navbar.php';
	    ?>
	    <br>
		<div class="col-md-6"  style="width:1000px; margin:0 auto;">
			<div class="card">
				<div class="card-header  bg-info text-white text-center">
					Register
				</div>
				<div class="card-body">
					<form class="" action="#" method="POST" onsubmit="return checkPassword();" >
						<b>Name: </b><input type="text" name="name" required class="form-control col-md-6">
						<b>Date Of Birth: </b><input type="date" name="dob" required class="form-control col-md-6">
						<b>Mobile Number: </b><input type="number" name="ph_no" id="ph_no" size="10" required class="form-control col-md-6">

						<b>Email: </b><input type="email" name="email" required class="form-control col-md-6">
						<b>Username: </b><input type="text" name="uname" id="uname" required class="form-control col-md-6">
						<b>Password: </b><input type="password" name="password" id="pass" required class="form-control col-md-6">
						<b>Re-Enter Password: </b><input type="password" name="passwordAgain" id="pass_again" required class="form-control col-md-6">
						<br>
						<input type="submit" name="subBtn" value="Register" class="btn btn-success">
						<input type="reset" name="resetBtn" value="Reset" class="btn btn-danger">
						<a href="index.php" class="btn btn-warning">Cancel</a>
					</form>
				</div>
			</div>
		</div>
		<?php
			if (isset($_POST['subBtn']))
			{
				$name = $_POST['name'];
				$user = $_POST['uname'];
				$password = $_POST['password'];
				$dob = $_POST['dob'];
				$ph_no = $_POST['ph_no'];
				$email = $_POST['email'];

				// $sql1q="INSERT INTO `users` (`username`, `password`, `date_of_birth`, `mobile_number`, `email`) VALUES ('".$user."', '".$password."', '".$dob."', '".$ph_no."', '".$email."'); ";
				$statement = $link->prepare("INSERT INTO `users` (`name`, `username`, `password`, `date_of_birth`, `phone_no`, `email`) VALUES (:name, :user, :password, :dob, :ph_no, :email); ");
				$insert_result = $statement->execute(['name' => $name, 'user' => $user, 'password' => $password, 'dob' => $dob, 'ph_no' => $ph_no, 'email' => $email]);
				if($insert_result)
				{
					echo '<div class="alert alert-success alert-fixed" role="alert">
							  Successfully Registered!
							  <br>
							  <a href="index.php">Click here</a> to go to Login Page
							</div>';
				}
				else
				{
					echo '<div class="alert alert-danger alert-fixed" role="alert">
							  Failed to Register!
							  <br>
							  Please Try Again
							</div>';
				}
			}
		?>
	</div>
</body>
</html>