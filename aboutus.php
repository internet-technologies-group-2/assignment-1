<!DOCTYPE html>
<html>
<head>
	<title>About Us</title>
</head>
<body>
	<div class="container">
		<?php
	    	include 'navbar.php';
	    ?>
		<header><h1><center>About Us</center></h1></header>
		<center>
		<table class='table table-bordered table-striped table-dark table-hover text-center'>
			<thead>
				<tr>
					<th>Person</th>
					<th>Name</th>
					<th>Roll NO</th>
				</tr>
			</thead>
			<div>
			<br>
			<tbody>
				<tr>
					<td><center><img src="https://www.idrbt.ac.in/PGDBT-SITE/images/201920/deepak.jpg" alt="Vishwesh" height="42" width="42"></center></td>
					<td>Vishwesh</td>
					<td>PGDBT201902</td>
				</tr>
				<tr>
					<td><center><img src="https://www.idrbt.ac.in/PGDBT-SITE/images/201920/sainadh.jpg" alt="Theja" height="42" width="42"></center></td>
					<td>Theja</td>
					<td>PGDBT201903</td>
				</tr>
				<tr>
					<td><center><img src="https://www.idrbt.ac.in/PGDBT-SITE/images/201920/sandeep.jpg" alt="Sandeep" height="42" width="42"></center></td>
					<td>Sandeep</td>
					<td>PGDBT201908</td>
				</tr>
				<tr>
					<td><center><img src="https://www.idrbt.ac.in/PGDBT-SITE/images/201920/naveen.jpg" alt="Naveen" height="42" width="42"></center></td>
					<td>Naveen</td>
					<td>PGDBT201911</td>
				</tr>
				<tr>
					<td><center><img src="https://www.idrbt.ac.in/PGDBT-SITE/images/201920/praveen.jpg" alt="Praveen" height="42" width="42"></center></td>
					<td>Praveen</td>
					<td>PGDBT201913</td>
				</tr>
				<tr>
					<td><center><img src="https://www.idrbt.ac.in/PGDBT-SITE/images/201920/tharun.jpg" alt="Tarun" height="42" width="42"></center></td>
					<td>Tarun</td>
					<td>PGDBT201916</td>
				</tr>
				
				
			</tbody>
		</table>
		</center>
	</div>
</body>
</html>