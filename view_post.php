<!DOCTYPE html>
<html>
<head>
	<title>View Post</title>
	<style type="text/css">
		.scrollable {
			height: 130px;
			overflow-y: auto;
		}
    </style>
</head>
<body>
	<div class="container">
        <?php
			include 'navbar.php';
			if (isset($_GET['post_id'])) {
				$post_id = $_GET['post_id'];
				$data = $link->prepare( 'SELECT * FROM posts WHERE post_id = '.$post_id );
				$data->execute();
				$sql_result = $data->fetchall();
				if (sizeof($sql_result) < 1) {
					echo '<div class="alert alert-danger" role="alert">
							No Post Available
						</div>';
				}

				$data1 = $link->prepare( 'SELECT * FROM comments WHERE post_id = '.$post_id.' ORDER BY comment_date DESC' );
				$data1->execute();
				$comments_result = $data1->fetchall();
			} 
		?>
		<br>
		<div class="col-md-12"  style="width:1000px; margin:0 auto;">
			<div class="card">
				<div class="card-header bg-info text-white text-center">
					View Post
					<a href="posts.php" class="btn btn-warning btn-sm float-right">Back</a>
				</div>
				<div class="card-body">
					<div class="card">
						<div class="card-body">
					<?php
						foreach ($sql_result as $value) {							
                            echo '<b>'.$value['post_title'].'</b><br>';
                            echo '<b>'.$value['post_description'].'</b></div></div><br>';
                            
                            if (sizeof($comments_result) > 0) {
                            	echo '<div class="card">
                            		<div class="card-img-top"><b>Comments:</b></div><br>
									<div class="card-body scrollable">';
	                            foreach ($comments_result as $comments_val) {
	                            	echo '<b>'.$comments_val['comment'].'</b><sub> by <b>'.$comments_val['comment_by'].'</b> @ '.$comments_val['comment_date'].'</sub><br>';
	                            }
	                            echo "</div></div><br>";
                            }

                            // echo 'created by '.$value['post_created_by'].' at '.$value['date_created'];
                            // if($value['date_updated'])
                            // {
                            //     echo '<br>updated at '.$value['date_updated'].'<br><br>';
                            // }
                            // else {
                            //     echo "<br><br>";
                            // }
                            ?>
                            
                            <form class="form-inline" method="POST" action="view_post.php">
                        		<input type="hidden" name="post_id_comm" value="<?= $value['post_id'] ?>">
                        		<input type="text" required name="comment" placeholder="Comment Here" class="form-control">
                        		&nbsp;&nbsp;
                        		<input type="submit" name="comment_submit" value="Submit" class="btn btn-primary">
                            </form>
                            <br>
                            <?php
                            if ($value['post_created_by'] == $display_name) {
                            	echo '<a href="update_post.php?post_id='.$value['post_id'].'" class="btn btn-success">Update</a>';
                            	echo '<a href="delete_post.php?post_id='.$value['post_id'].'" class="btn btn-danger float-right">Delete</a><br>';
                            }
						}
					?>
				</div>
			</div>
		</div>

		<?php
			if (isset($_POST['comment_submit']))
			{
				$post_id = $_POST['post_id_comm'];
				$comment = $_POST['comment'];
				$current_user = $display_name;

				$statement = $link->prepare("INSERT INTO `comments`(`post_id`, `comment`, `comment_by`) VALUES (:post_id,:comment,:comm_by)");
				$insert_result = $statement->execute(['post_id' => $post_id, 'comment' => $comment, 'comm_by' => $current_user]);
				if($insert_result)
				{
					echo '<div class="alert alert-success alert-fixed" role="alert">
								Successfully commented on post!
							</div>';
				}
				else
				{
					echo '<div class="alert alert-danger alert-fixed" role="alert">
								Failed to comment on post!
								<br>
								Please Try Again
							</div>';
				}
				echo '<script>location.href = "view_post.php?post_id='.$post_id.'";</script>';
			}
		?>
	</div>
</body>
</html>