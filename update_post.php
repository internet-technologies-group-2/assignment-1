<!DOCTYPE html>
<html>
<head>
	<title>Update Post</title>
</head>
<body>
	<div class="container">
		<?php
            include 'navbar.php';
            
            $post_id = $_GET['post_id'];
			$data = $link->prepare( 'SELECT * FROM posts WHERE post_id = '.$post_id );
			$data->execute();
			$sql_result = $data->fetchall();
			if (sizeof($sql_result) < 1) {
				echo '<div class="alert alert-danger" role="alert">
						No Post Available
					</div>';
			} else {
                foreach ($sql_result as $value) {
                    $title = $value['post_title'];
                    $description = $value['post_description'];
                }
            }
		?>
		<br>
		<div class="col-md-6"  style="width:1000px; margin:0 auto;">
			<div class="card">
				<div class="card-header  bg-info text-white text-center">
					Update Post
				</div>
				<div class="card-body">
					<form class="form-group" action="#" method="POST">
						<b>Title: </b><input type="text" name="title" required value="<?= $title; ?>" class="form-control col-md-12">
						<b>Description: </b>
						<textarea rows="4" cols="50"  name="description" required class="form-control col-md-12"><?= $description; ?></textarea>	
                        <br>
                        <input type="hidden" name="post_id" value="<?= $post_id; ?>">
						<input type="submit" name="subBtn" value="Update" class="btn btn-warning">
						<a href="view_post.php?post_id=<?= $post_id; ?>" class="btn btn-danger float-right">Cancel</a>
					</form>
				</div>
			</div>
		</div>
	</div>
	<?php
		if (isset($_POST['subBtn']))
		{
			$title = $_POST['title'];
            $description = $_POST['description'];
            $post_id = $_POST['post_id'];
            
            $sql = "UPDATE posts SET post_title=?, post_description=?, date_updated=? WHERE post_id=?";
            $insert_result = $link->prepare($sql)->execute([$title, $description, date("Y-m-d h:i:s"), $post_id]);

			if($insert_result)
			{
				echo '<div class="alert alert-success alert-fixed" role="alert">
                            Successfully updated post!
                            
                            <script>location.href = "view_post.php?post_id='.$post_id.'";</script>
						</div>';
			}
			else
			{
				echo '<div class="alert alert-danger alert-fixed" role="alert">
							Failed to update post!
							<br>
							Please Try Again
						</div>';
			}
		}
	?>
</body>
</html>