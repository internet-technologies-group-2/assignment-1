<link rel="stylesheet" href="bootstrap.css">
<style type="text/css">
	body {
		background-color: lavender;
	}

	.alert-fixed {
		position:fixed; 
		top: 0px; 
		left: 0px; 
		width: 100%;
		z-index:9999; 
		border-radius:0px
	}
</style>
<?php
	session_start();
	include 'config.php';
	if (!isset($_SESSION["uname"]))
	{
		$display_name = "Guest";
		$display = false;
		$homepage_link = "index.php";
	}
	else
	{
		$display = true;
		$display_name = $_SESSION["uname"];
		$homepage_link = "home.php";
	}
?>
<nav class="navbar navbar-expand-sm bg-primary navbar-dark">
    <a class="navbar-brand" href="<?= $homepage_link ?>">Team 2</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div id="navbarNavDropdown" class="navbar-collapse collapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="<?= $homepage_link ?>">Home</a>
            </li>
            <?php 
            	if ($display) { 
            		echo '<li class="nav-item">
		                <a class="nav-link" href="posts.php">Posts</a>
		            </li>';
            	}
            ?>
            <li class="nav-item">
                <a class="nav-link" href="aboutus.php">About Us</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="contact_us.php">Contact Us</a>
            </li>

        </ul>
        <ul class="navbar-nav">
            <li class="nav-item active">
                <div class="nav-link">Welcome <?= $display_name ?></div>
            </li>
            <?php 
            	if ($display) { 
            		echo '<li class="nav-item active">
		                <a class="nav-link" href="logout.php" id="logoutBtn" onclick="return confirm(\'Are you Sure to Logout ?\');">Logout</a>
		            </li>';
            	} else {
		            echo '<li class="nav-item active">
		                <a class="nav-link" href="login.php">Login</a>
		            </li><li class="nav-item active">
		                <a class="nav-link" href="signup_page.php">Register</a>
		            </li>';
            	}
            ?>
            
        </ul>
    </div>
</nav>