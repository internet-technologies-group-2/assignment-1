<!DOCTYPE html>
<html>
<head>
	<title>Contact Page</title>
 
</head>
<body>
	<div class="container">
		<?php
	    	include 'navbar.php';
	    ?>
	    <br>
		<div class="col-md-6"  style="width:1000px; margin:0 auto;">
			<div class="card">
				<div class="card-header  bg-info text-white text-center">
					Contact Us
				</div>
				<div class="card-body">
					<form class="" action="#" method="POST" onsubmit="return checkPassword();" >
						<b>Name: </b><input type="text" name="name" required class="form-control col-md-6">
			
						<b>Mobile Number: </b><input type="number" name="ph_no" id="ph_no" size="10" required class="form-control col-md-6">

						<b>Email: </b><input type="email" name="email" required class="form-control col-md-6">
                        <b>Description: </b>
                        <textarea name="message" required class="form-control col-md-6"></textarea>
                       
						<br>
						<input type="submit" name="subBtn" value="Submit" class="btn btn-success">
						<input type="reset" name="resetBtn" value="Reset" class="btn btn-danger">
						<a href="index.php" class="btn btn-warning">Cancel</a>
					</form>
				</div>
			</div>
		</div>
		<?php
			if (isset($_POST['subBtn']))
			{
				$name = $_POST['name'];
				$ph_no = $_POST['ph_no'];
                $email = $_POST['email'];
                $description=$_POST['message'];

				// $sql1q="INSERT INTO `users` (`username`, `password`, `date_of_birth`, `mobile_number`, `email`) VALUES ('".$user."', '".$password."', '".$dob."', '".$ph_no."', '".$email."'); ";

				$statement = $link->prepare("INSERT INTO contact_us (`name`,  `phone_no`, `email`,`description`) VALUES (:user,:ph_no, :email,:description); ");
				$insert_result = $statement->execute(['user' => $name,  'ph_no' => $ph_no, 'email' => $email,'description'=>$description]);
				if($insert_result)
				{
					echo '<div class="alert alert-success alert-fixed" role="alert">
							 Contacted  Successfully 
														  
							</div>';
				}
				else
				{
					echo '<div class="alert alert-danger alert-fixed" role="alert">
							  Failed to Contact!
							  <br>
							  Please Try Again
							</div>';
				}
			}
		?>
	</div>
</body>
</html>