<!DOCTYPE html>
<html>
<head>
	<title>Delete</title>
</head>
<body>
	<div class="container">
		<?php
            include 'navbar.php';
            
            $post_id = $_GET['post_id'];
            
            $sql = "DELETE from posts WHERE post_id=?";
            $insert_result = $link->prepare($sql)->execute([$post_id]);

			if($insert_result)
			{
				echo '<div class="alert alert-success alert-fixed" role="alert">
                            Successfully deleted post!
                            
                            <script>location.href = "posts.php";</script>
						</div>';
			}
			else
			{
				echo '<div class="alert alert-danger alert-fixed" role="alert">
							Failed to delete post!
							<br>
                            Please Try Again
                            
                            <script>location.href = "view_post.php?post_id='.$post_id.'";</script>
						</div>';
			}
		?>
		<!--    -->
</body>
</html>